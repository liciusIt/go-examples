package main

import "fmt"

func main() {
	//Array estatico de 5 elementos de tipo enteros
	var numeros [5]int
	fmt.Println(numeros)

	numeros[0] = 10
	numeros[1] = 20
	numeros[2] = 30
	numeros[3] = 40

	fmt.Println(numeros)

	fmt.Println(numeros[4])

	//Arrays con valores
	nombres := [3]string{"Marcelo", "Alejandro", "Albarracin"}

	fmt.Println(nombres)

	//Arays dinamicos con valores
	colores := [...]string{
		"Rojo",
		"Verde",
		"Negro",
		"Azul",
	}

	fmt.Println(colores, len(colores))

	//Arrays con indices definidos
	momedas := [...]string{
		0: "Dolar",
		1: "Pesos Argentinos",
		2: "Soles",
		3: "Libras",
		4: "Pesos Mexicanos",
		5: "Euro",
	}

	fmt.Println(momedas, len(momedas))

	//sub Array, desde la posición inicial hasta la pocicion 3
	subMonedaIni := momedas[:3]
	fmt.Println(subMonedaIni)

	//sub Array, desde la posición 3 hasta el final
	subMonedaEnd := momedas[3:]
	fmt.Println(subMonedaEnd)

}
