package main

import "fmt"

// Función que calcula el igv
func calcularIgv(precio, igv float64) float64 {
	return igv * precio
}

// Funcion que calcula percio de venta
func precioVenta(precio, igv float64) float64 {
	return precio + igv
}

func main() {

	//Variables
	var precio, igv, precioVen float64

	//Entrada  de datos
	fmt.Print("Ingrese Valor de Venta: ")
	fmt.Scanln(&precio)

	//Calcular igv
	igv = calcularIgv(precio, 0.18)

	//Calcular precio de venta
	precioVen = precioVenta(precio, igv)

	//Salida de datos
	fmt.Println("==========================")
	fmt.Println("Valor de Venta:", precio)
	fmt.Println("IGV:", igv)
	fmt.Println("Precio de Venta:", precioVen)
	fmt.Println("==========================")

}
