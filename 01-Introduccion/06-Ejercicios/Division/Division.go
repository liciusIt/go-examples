package main

import "fmt"

// función que calcula cociente
func cociente(a, b int) int {
	return a / b
}

// Función que calcula residuo
func residuo(a, b int) int {
	return a % b
}

func main() {

	a := 0
	b := 0
	cocie := 0
	resto := 0

	fmt.Print("Ingrese el numero para la dividendo: ")
	fmt.Scan(&a)

	fmt.Print("Ingrese el numero para el cociente: ")
	fmt.Scan(&b)

	cocie = cociente(a, b)

	fmt.Println("El cociente de la division es: ", cocie)

	resto = residuo(a, b)

	fmt.Println("El resto de la division es: ", resto)
}
