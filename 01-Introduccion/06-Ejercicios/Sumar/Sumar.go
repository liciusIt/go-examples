package main

import "fmt"

// sumar función de sumar dos numeros
func sumar(a, b int) int {
	return a + b
}

func main() {

	a := 0
	b := 0

	fmt.Print("Ingrsar primer numero:")
	fmt.Scan(&a)
	fmt.Print("Ingresar segundo numero:")
	fmt.Scan(&b)

	resultado := sumar(a, b)

	fmt.Println("El resultado de la suma es: ", resultado)
}
