package main

import "fmt"

func main() {

	a := 10
	b := 20

	//Suma
	resultado := a + b
	fmt.Println("Suma: ", resultado)

	//Resta
	resultado = b - a
	fmt.Println("Resta: ", resultado)

	//Multiplicacion
	resultado = a * b
	fmt.Println("Multiplicacion: ", resultado)

	//Division Entero
	resultado = a / b
	fmt.Println("Division Entero: ", resultado)

	//Division Flotante
	division := 2.0 / 3.0
	fmt.Println("Division Flotante: ", division)

	//Resto Division
	resultado = 2 % 3
	fmt.Println("Division Resto: ", resultado)

}
