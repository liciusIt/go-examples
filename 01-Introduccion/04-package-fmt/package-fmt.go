package main

import "fmt"

func main() {

	nombre := "Marcelo"
	edad := 43

	//Print con salto de linea
	fmt.Println("Nombre:", nombre)
	fmt.Println("Edad:", edad)

	//Formatear Strig %s(tipo string) %d(tipo int)
	fmt.Printf("Hola %s y su edad es %d\n", nombre, edad)

	//Formatear String %v cuando no sabemos el tipo de dato
	fmt.Printf("Hola %v y su edad es %v\n", nombre, edad)

	mensaje := fmt.Sprintf("Hola %s y su edad es %d\n", nombre, edad)

	fmt.Println(mensaje)

	//Ingresar por consola
	fmt.Print("Ingrese su Nombre:")
	fmt.Scan(&nombre)
	fmt.Println("Nombre ingresado: ", nombre)
}
