package main

import "fmt"

func main() {

	var variable1 string
	variable1 = "Marcelo"
	var variable2 string = "Albarracin"
	edad := 42

	fmt.Println(variable1, variable2, edad)

	var a1 int
	var a2 float64
	var a3 bool
	var a4 string

	fmt.Println(a1, a2, a3, a4)

	const p = 3.14
	fmt.Println(p)
}
