package main

import "fmt"

// Funcion sin parametros
func saludar() {
	fmt.Println("Hola Mundo")
}

// Funcion con Parametros
func bienvenido(nombre string) {
	fmt.Printf("Hola %v, buenos dias.\n", nombre)
}

// Funcion con retorno
func sumar(a, b int) int {
	return a + b
}

func main() {
	saludar()
	bienvenido("Marcelo")

	resultado := sumar(20, 30)
	fmt.Println("El resultado de la suma es:", resultado)
}
